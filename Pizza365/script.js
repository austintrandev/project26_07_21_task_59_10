var gSelectedMenuStructure = {
    menusize: [{
        menuName: "",
        duongKinhCM: 0,
        suonNuong: 0,
        saladGr: 0,
        drink: 0,
        priceVND: 0
    }],
    /***
    * Hàm set các actioc khi click vào các nút menucombo
    * Input: paramGetButtonMeunuCombo: value của nút được chọn
    * Set color và truyền thông tin menu được chọn vào biến gSelectedMenuStructure
    */
    setActionOnClickComboMenu: function (paramGetButtonMeunuCombo) {
        switch (paramGetButtonMeunuCombo) {
            case "S":
                $("#btn-size-s").removeClass("btn-warning");
                $("#btn-size-s").addClass("btn-success");
                $("#btn-size-m").removeClass("btn-success");
                $("#btn-size-m").addClass("btn-warning");
                $("#btn-size-l").removeClass("btn-success");
                $("#btn-size-l").addClass("btn-warning");
                this.menusize.menuName = "S";
                this.menusize.duongKinhCM = 20;
                this.menusize.suonNuong = 2;
                this.menusize.saladGr = 200;
                this.menusize.drink = 2;
                this.menusize.priceVND = 150000;
                console.log(gSelectedMenuStructure);
                break;
            case "M":
                $("#btn-size-s").removeClass("btn-success");
                $("#btn-size-s").addClass("btn-warning");
                $("#btn-size-m").removeClass("btn-warning");
                $("#btn-size-m").addClass("btn-success");
                $("#btn-size-l").removeClass("btn-success");
                $("#btn-size-l").addClass("btn-warning");
                this.menusize.menuName = "M";
                this.menusize.duongKinhCM = 25;
                this.menusize.suonNuong = 4;
                this.menusize.saladGr = 300;
                this.menusize.drink = 3;
                this.menusize.priceVND = 200000;
                console.log(gSelectedMenuStructure);
                break;
            case "L":
                $("#btn-size-s").removeClass("btn-success");
                $("#btn-size-s").addClass("btn-warning");
                $("#btn-size-m").removeClass("btn-success");
                $("#btn-size-m").addClass("btn-warning");
                $("#btn-size-l").removeClass("btn-warning");
                $("#btn-size-l").addClass("btn-success");
                this.menusize.menuName = "L";
                this.menusize.duongKinhCM = 30;
                this.menusize.suonNuong = 8;
                this.menusize.saladGr = 500;
                this.menusize.drink = 4;
                this.menusize.priceVND = 250000;
                console.log(gSelectedMenuStructure);
                break;
            default:
                console.assert(false, "90: Không đúng các size M, L, S");
                throw ("100: Trường hợp đặc biệt, ko đúng các Size M, L, S");
                break;
        }
    }

}

/***
 * Hàm set các actioc khi click vào các nút Pizzatype
 * Input: paramGetButtonPizzaType: value của nút được chọn
 * Set color và truyền thông tin menu được chọn vào biến gSelectedPizzaType
 */
var gSelectedPizzaType = {
    pizzatype: [{
        TypeName: "",
        contains: ""
    }],
    setActionOnClickPizzaType: function (paramGetButtonPizzaType) {
        switch (paramGetButtonPizzaType) {
            case "Hawaii":
                $("#btn-pizza-hawaii").removeClass("btn-warning");
                $("#btn-pizza-hawaii").addClass("btn-success");
                $("#btn-pizza-sea-food").removeClass("btn-success");
                $("#btn-pizza-sea-food").addClass("btn-warning");
                $("#btn-pizza-bacon").removeClass("btn-success");
                $("#btn-pizza-bacon").addClass("btn-warning");
                this.pizzatype.TypeName = "hawaii";
                this.pizzatype.contains = "Xốt cà chua, phô mai Mozzarella, thịt dăm bông, thơm"
                console.log(gSelectedPizzaType);
                break;
            case "Seafood":
                $("#btn-pizza-hawaii").removeClass("btn-success");
                $("#btn-pizza-hawaii").addClass("btn-warning");
                $("#btn-pizza-sea-food").removeClass("btn-warning");
                $("#btn-pizza-sea-food").addClass("btn-success");
                $("#btn-pizza-bacon").removeClass("btn-success");
                $("#btn-pizza-bacon").addClass("btn-warning");
                this.pizzatype.TypeName = "seafood";
                this.pizzatype.contains = "Xốt cà chua, phô mai Mozzarella, tôm, mực, thanh cua, hành tây"
                console.log(gSelectedPizzaType);
                break;
            case "Bacon":
                $("#btn-pizza-hawaii").removeClass("btn-success");
                $("#btn-pizza-hawaii").addClass("btn-warning");
                $("#btn-pizza-sea-food").removeClass("btn-success");
                $("#btn-pizza-sea-food").addClass("btn-warning");
                $("#btn-pizza-bacon").removeClass("btn-warning");
                $("#btn-pizza-bacon").addClass("btn-success");
                this.pizzatype.TypeName = "bacon";
                this.pizzatype.contains = "Xốt cà chua,thịt gà, thịt heo muối, phô mai Mozzarella, cà chua"
                console.log(gSelectedPizzaType);
                break;
            default:
                console.assert(false, "90: Không đúng các Type Hawai, Seafood, Bacon");
                throw ("100: Trường hợp đặc biệt, ko đúng các Type Hawai, Seafood, Bacon");
                break;
        }
    }
}

var gUserDataObject = {
    orderinfo: [{
        fullname: "",
        email: "",
        phonenumber: "",
        address: "",
        note: "",
        voucherid: ""
    }],
    /***
    * Hàm đọc các dữ liệu input truyền vào ObjectData
    * Input: paramObjectData : chứa các thuộc tính input
    * Output: truyền các input đã được cắt các khoảng trắng (trim) truyền vào paramObjectData
    */
    readDataInput: function () {
        this.orderinfo.fullname = $("#inp-name").val();
        this.orderinfo.email = $("#inp-email").val();
        this.orderinfo.phonenumber = $("#inp-phone-number").val();
        this.orderinfo.address = $("#inp-address").val();
        this.orderinfo.note = $("#inp-note").val();
        this.orderinfo.voucherid = $("#inp-voucher-id").val();
    },
    /***
    * Hàm check các ký tự dặc biệt
    * Input: paramText : input username của user
    * Output: trả về true nếu input không chứa các ký tự đặc biệt và ngược lại
    */
    isCheckCharacters: function (paramText) {
        var format = /[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]+/;
        var vCheck = true;
        if (format.test(paramText)) { // neu paramText khong hop le
            vCheck = false;
        }
        return vCheck;
    },

    /***
    * Hàm validate các input của user
    * Input: paramObjectData : chứa các thuộc tính firstname, lastname, age, email
    * Output: trả về true nếu input không chứa các ký tự đặc biệt và ngược lại sẽ thông báo các alert tương ứng với các lỗi input
    */
    validateInputData: function (paramObjectData) {
        var vCheckEmail1 = "@";
        var vCheckEmail2 = "."
        var vMessage = "";
        if (paramObjectData.fullname.length === 0) {
            vMessage = "Xin hãy nhập họ tên khách hàng!!";
            console.assert(false, '310: ' + vMessage);
            alert(vMessage);
            $("#inp-name").focus();
            throw vMessage;
        }
        else if (!this.isCheckCharacters(paramObjectData.fullname)) {
            vMessage = "Họ tên không được nhập ký tự đặc biệt";
            console.assert(false, '320: ' + vMessage);
            alert(vMessage);
            $("#inp-name").focus();
            throw vMessage;
        }
        else if (paramObjectData.email.length === 0) {
            vMessage = "Xin hãy nhập email!!";
            $("#inp-email").focus();
            console.assert(false, '310: ' + vMessage);
            alert(vMessage);
            throw vMessage;
        }
        else if (paramObjectData.email.indexOf(vCheckEmail1) === -1 || (paramObjectData.email.lastIndexOf(vCheckEmail2) - paramObjectData.email.indexOf(vCheckEmail1)) < 2) {
            vMessage = "Email không đúng định dạng";
            console.assert(false, '330: ' + vMessage);
            alert(vMessage);
            $("#inp-email").focus();
            throw vMessage;
        }
        else if (paramObjectData.phonenumber.length === 0) {
            vMessage = "Xin hãy nhập số điện thoại!!";
            console.assert(false, '310: ' + vMessage);
            alert(vMessage);
            $("#inp-phone-number").focus();
            throw vMessage;
        }
        else if (isNaN(paramObjectData.phonenumber)) {
            vMessage = "Điện thoại phải nhập dạng number!!";
            console.assert(false, '320: ' + vMessage);
            alert(vMessage);
            $("#inp-phone-number").focus();
            throw vMessage;
        }
        else if (paramObjectData.address.length === 0) {
            vMessage = "Xin hãy nhập địa chỉ giao hàng!!";
            console.assert(false, '310: ' + vMessage);
            alert(vMessage);
            $("#inp-address").focus();
            throw vMessage;
        }
        else if (paramObjectData.voucherid.length === 0) {
            vMessage = "Xin hãy nhập voucherId!!";
            console.assert(false, '310: ' + vMessage);
            alert(vMessage);
            $("#inp-voucher-id").focus();
            throw vMessage;
        }
        else if (!this.isCheckCharacters(paramObjectData.voucherid)) {
            vMessage = "VoucherId không chứa kí tự đặc biệt";
            console.assert(false, '320: ' + vMessage);
            alert(vMessage);
            $("#inp-voucher-id").focus();
            throw vMessage;
        }
        else if (gSelectedMenuStructure.menusize.menuName === undefined) {
            vMessage = "Vui lòng chọn loại menu";
            //console.log(gSelectedMenuStructure.menusize.menuName);
            console.assert(false, '320: ' + vMessage);
            alert(vMessage);
            document.documentElement.scrollTo($("#block-combo-pizza").position());
            throw vMessage;
        }
        else if (gSelectedPizzaType.pizzatype.TypeName === undefined) {
            vMessage = "Vui lòng chọn loại Pizza";
            //console.assert(false, '320: ' + vMessage);
            alert(vMessage);
            document.documentElement.scrollTo($("#block-pizza-type").position());
            throw vMessage;
        }
        else if ($("#select-drink").val() === "") {
            vMessage = "Vui lòng chọn loại nước uống";
            //console.assert(false, '320: ' + vMessage);
            alert(vMessage);
            document.documentElement.scrollTo($("#block-drink").position());
            throw vMessage;
        }
        else {
            return true;
        }
    }
}
var gObjectRequest = {
    datarequest: [{
        kichCo: "",
        duongKinh: 0,
        suon: 0,
        salad: 0,
        loaiPizza: "",
        idVourcher: "",
        idLoaiNuocUong: "",
        soLuongNuoc: 0,
        hoTen: "",
        thanhTien: 0,
        email: "",
        soDienThoai: "",
        diaChi: "",
        loiNhan: ""
    }],
    setObjectDataRequest: function () {
        this.datarequest.kichCo = gSelectedMenuStructure.menusize.menuName;
        this.datarequest.duongKinh = gSelectedMenuStructure.menusize.duongKinhCM;
        this.datarequest.suon = gSelectedMenuStructure.menusize.suonNuong;
        this.datarequest.salad = gSelectedMenuStructure.menusize.saladGr;
        this.datarequest.loaiPizza = gSelectedPizzaType.pizzatype.TypeName;
        this.datarequest.idVourcher = gUserDataObject.orderinfo.voucherid;
        this.datarequest.idLoaiNuocUong = $("#select-drink").val();
        this.datarequest.soLuongNuoc = gSelectedMenuStructure.menusize.drink;
        this.datarequest.hoTen = gUserDataObject.orderinfo.fullname;
        this.datarequest.thanhTien = gSelectedMenuStructure.menusize.priceVND;
        this.datarequest.email = gUserDataObject.orderinfo.email;
        this.datarequest.soDienThoai = gUserDataObject.orderinfo.phonenumber;
        this.datarequest.diaChi = gUserDataObject.orderinfo.address;
        this.datarequest.loiNhan = gUserDataObject.orderinfo.note;
    },
    setDataToModalInput: function(){
        var vThanhTien = gObjectRequest.datarequest.thanhTien;
        //console.log("2: ",gDiscount);
        gThanhToan = (vThanhTien - (vThanhTien*gDiscount/100));
        $("#modal-inp-full-name").val(this.datarequest.hoTen);
        $("#modal-inp-phone-number").val(this.datarequest.soDienThoai);
        $("#modal-inp-address").val(this.datarequest.diaChi);
        $("#modal-inp-note").val(this.datarequest.loiNhan);
        $("#modal-inp-voucher-id").val(this.datarequest.idVourcher);
        $("#modal-textarea-detail-form").append("Xác nhận: " + this.datarequest.hoTen + " ; " + this.datarequest.soDienThoai + " ; " + this.datarequest.diaChi 
                                                + "\n" + "Menu: " + this.datarequest.kichCo + ", sườn nướng: " + this.datarequest.suon + ", nước: " + this.datarequest.soLuongNuoc + "..." 
                                                + "\n" + "Loại pizza: " + this.datarequest.loaiPizza + ", Giá: ", this.datarequest.thanhTien + ", Mã giảm giá: " + this.datarequest.idVourcher 
                                                + "\n" + "Phải thanh toán: " + gThanhToan + " (giảm giá: " + gDiscount + "%)");
    }

}