package com.pizza.campaign.api;

public class CPromotionByDay {
	private String day;
	public CPromotionByDay() {
		
	}
	public static String getPromtionByDay(String day) {
		String promotion = "";
		switch (day) {
		case "Thứ Hai":
			promotion = "Mua 1 tặng 1!!";
			break;
		case "Thứ Ba":
			promotion = "Tặng tất cả khách hàng 1 phần bánh ngọt!!";
			break;
		case "Thứ Tư":
			promotion = "Giảm 10% cho tất cả các đơn hàng!!";
			break;
		case "Thứ Năm":
			promotion = "Miễn phí nước uống cho đơn hàng trên 200k!!";
			break;
		case "Thứ Sáu":
			promotion = "Tặng thêm 1 phần kem tươi cho tất cả combo!!";
			break;
		case "Thứ Bảy":
			promotion = "Giảm 20% cho combo gia đình (Combo L)!!";
			break;
		case "Chủ Nhật":
			promotion = "Giảm 20% cho tất cả các combo pizza!!";
			break;

		default:
			break;
		}
		return promotion;
	}
}
