package com.pizza.campaign.api;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CDailyCampaign {
	CPromotionByDay pizzaPromotion = new CPromotionByDay();
	@CrossOrigin
	@GetMapping("")
	public String getDateViet() {
		DateTimeFormatter dtfVietnam = DateTimeFormatter.ofPattern("EEEE").localizedBy(Locale.forLanguageTag("vi"));
		LocalDate today = LocalDate.now(ZoneId.systemDefault());
		return String.format("%s : %s", dtfVietnam.format(today),pizzaPromotion.getPromtionByDay(dtfVietnam.format(today)));
	}
}
